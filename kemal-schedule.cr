require "kemal"
require "cron_parser"

before_all { |http| http.response.content_type = "application/json"  }

post "/"   { |http| process http }

error 404  { |http|
    http.response.content_type = "application/json"
    r = http.request
    routes = ["POST /"]
    {
        error:   404             ,
        method:  "#{r.method}"   ,
        path:    "#{r.path}"     ,
        supported_routes: routes ,
        message: "no such route #{r.method} #{r.path}"
    }.to_json
}

begin
    env   = ENV["ENV"]  ||= "production"
    bind  = ENV["BIND"] ||= "0.0.0.0"
    port  = ENV["PORT"] ||= "8080"
    port = port.to_i
    Kemal.run { |config|
      server = config.server.not_nil!
      server.bind_tcp bind, port
      config.env = env
    }
rescue error
    STDERR.puts error.message
    exit 1
end

class Schedule
    include JSON::Serializable
    property start : Array(String), pause : Array(String)
end

def process(http : HTTP::Server::Context)
    params = http.params.query.to_h
    begin
        body    = http.request.body.as(IO).gets_to_end
        Schedule.from_json body.not_nil!
        config  = JSON.parse(body)
        params.has_key?("ts")     && ( time = Time.parse_rfc3339(params["ts"]) ) || ( time = Time.utc() )
        params.has_key?("format") && ( format = params["format"] )               || ( format = "short" )
        now         = time.to_rfc3339
        day_of_week = time.day_of_week.to_s
        parsed = parse_schedules(config,time)
        state  = parsed.as(Hash).["state"]
        next_  = parsed.as(Hash).["next"]
        last   = parsed.as(Hash).["last"]
        format == "full" && ( schedules = {"state" => state, "payload" => { "new_status" => state }, "now" => now, "day_of_week" => day_of_week, "next" => next_, "last" => last, "error" => false})
        format != "full" && ( schedules = {"new_status" => state, "error" => false} )
        response = schedules
    rescue error : ArgumentError|JSON::ParseException|KeyError|TypeCastError|JSON::SerializableError
        http.response.status_code = 400
        response = {error: true, message: error.message.to_s.split("\n").first, payload_received: config||body}
    end
        params.has_key?("pretty") && ( return response.to_pretty_json ) || ( return response.to_json )
end

def parse_schedules(config : JSON::Any, time : Time)
    last_start = [] of String
    config["start"].as_a.each { |schedule| last_start.push(CronParser.new(schedule.as_s).last(time.shift(59,0)).to_rfc3339)}
    last_start = last_start.sort.last
    last_pause = [] of String
    config["pause"].as_a.each { |schedule| last_pause.push(CronParser.new(schedule.as_s).last(time.shift(59,0)).to_rfc3339)}
    last_pause = last_pause.sort.last
    next_start = [] of String
    config["start"].as_a.each { |schedule| next_start.push(CronParser.new(schedule.as_s).next(time.shift(-59,0)).to_rfc3339)}
    next_start = next_start.sort.first
    next_pause = [] of String
    config["pause"].as_a.each { |schedule| next_pause.push(CronParser.new(schedule.as_s).next(time.shift(-59,0)).to_rfc3339)}
    next_pause = next_pause.sort.first
    now = time.to_rfc3339
    state = "paused" if [last_pause,last_start].max == last_pause
    state = "live"   if [last_pause,last_start].max == last_start
    state = "paused" if now == next_pause
    state = "live"   if now == next_start
    return {
        "last"  => { "start" => last_start, "pause" => last_pause } ,
        "next"  => { "start" => next_start, "pause" => next_pause } ,
        "state" => state
    }
end
