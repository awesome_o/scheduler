# web-scheduler

#### description

A simple web-based crystal-lang app to check versus multiple cron schedules whether a task should run or not

#### usage

`POST` a `JSON` payload containing schedules in crontab format

See `scheduler.yml` & examples below for expected format

Will respond w/ status - either `live` or `paused` - based on the crontab schedules provided

All times `UTC`

Uses `now()` by default but `ts` query param can be provided to test schedule versus other timestamp _(in `RFC3339` format)_

Output can also be expanded to provide more details using `format=full` query param.

#### examples

Configure `Heroku` `URL` & example `JSON` payload

    URL="https://synthetics-scheduler.herokuapp.com"

    json_payload(){
    cat << HEREDOC
    {
      "start": [
        "00 08 * * 1,2,3,4,5"
      ],
      "pause": [
        "00 18 * * 1,2,3,4,5",
        "00 00 * * 6,7"
      ]
    }
    HEREDOC
    }

Basic example

	# curl -Lksw"\n" "${URL}" -XPOST -d "$(json_payload)"
	{
	  "new_status": "live"
	}

Full Output

	# curl -Lksw"\n" "${URL}?format=full" -XPOST -d "$(json_payload)"
	{
	  "state": "live",
	  "payload": {
	    "new_status": "live"
	  },
	  "now": "2022-01-18T14:48:54Z",
	  "day_of_week": "Tuesday",
	  "next": {
	    "start": "2022-01-19T08:00:00Z",
	    "pause": "2022-01-18T18:00:00Z"
	  },
	  "last": {
	    "start": "2022-01-18T08:00:00Z",
	    "pause": "2022-01-17T18:00:00Z"
	  }
	}

Specifying TimeStamp 

_Test schedules edge-case #1_

	# curl -Lksw"\n" "${URL}?format=full&ts=2022-01-01T07:59:59" -XPOST -d "$(json_payload)"
	{
	  "state": "live",
	  "payload": {
	    "new_status": "live"
	  },
	  "now": "2022-01-18T14:49:40Z",
	  "day_of_week": "Tuesday",
	  "next": {
	    "start": "2022-01-19T08:00:00Z",
	    "pause": "2022-01-18T18:00:00Z"
	  },
	  "last": {
	    "start": "2022-01-18T08:00:00Z",
	    "pause": "2022-01-17T18:00:00Z"
	  }
	}

_Test schedules edge-case #2_

    # curl -Lksw"\n" "${URL}?format=full&ts=2022-01-01T08:00:00" -XPOST -d "$(json_payload)"

_Test schedules edge-case #3_

    # curl -Lksw"\n" "${URL}?format=full&ts=2022-01-01T17:59:59" -XPOST -d "$(json_payload)"

_Test schedules edge-case #4_

    # curl -Lksw"\n" "${URL}?format=full&ts=2022-01-01T18:00:00" -XPOST -d "$(json_payload)"
