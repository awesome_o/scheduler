require "cron_parser"
require "json"
require "yaml"

begin
    config_file = ENV[ "config" ] ||= "scheduler.yml"
    config = File.open( config_file ) { | file |
        YAML.parse( file )
    }
rescue error
    STDERR.puts error
    exit 1
end

begin
  time = Time.parse_rfc3339(ARGV.first)
rescue
  time = Time.utc()
end

now = time.to_rfc3339
STDERR.puts now
day_of_week = time.day_of_week.to_s

last_start = [] of String
config.as_h["start"].as_a.each { | schedule |
    last_start.push( CronParser.new( schedule.as_s ).last( time.shift(59,0) ).to_rfc3339 )
}
last_start = last_start.sort.last

last_pause = [] of String
config.as_h["pause"].as_a.each { | schedule |
    last_pause.push( CronParser.new( schedule.as_s ).last( time.shift(59,0) ).to_rfc3339 )
}
last_pause = last_pause.sort.last

next_start = [] of String
config.as_h["start"].as_a.each { | schedule |
    next_start.push( CronParser.new( schedule.as_s ).next( time.shift(-59,0) ).to_rfc3339 )
}
next_start = next_start.sort.first

next_pause = [] of String
config.as_h["pause"].as_a.each { | schedule |
    next_pause.push( CronParser.new( schedule.as_s ).next( time.shift(-59,0) ).to_rfc3339 )
}
next_pause = next_pause.sort.first

if [last_pause,last_start].max == last_start
    state = "live"
else
    state = "paused"
end

if now == next_pause
    state = "paused"
end
if now == next_start
    state = "live"
end

ENV[ "format" ] ||= "short"
if ENV[ "format" ] == "full"
    schedules = {
      "state"   => state,
      "payload" =>
        {
          "new_status" => state
        },
      "now"     => now,
      "day_of_week" => day_of_week,
      "next"    =>
        {
          "start" => next_start,
          "pause" => next_pause
        },
      "last"  =>
        {
          "start" => last_start,
          "pause" => last_pause
        }
    }
    puts schedules.to_pretty_json
else
    schedules = {
      "new_status" => state
    }
    puts schedules.to_json
end
