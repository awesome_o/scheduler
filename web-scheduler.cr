require "cron_parser"
require "json"
require "yaml"
require "http/server"

DEBUG = ENV["DEBUG"] ||= "true"

bind  = ENV["BIND"]  ||= "0.0.0.0"
port  = ENV["PORT"]  ||= "8080"
port  = port.to_i

server = HTTP::Server.new do |http|
    http.response.print request_parser(http)
    http.response.close
end

address = server.bind_tcp bind, port
server.listen

def request_parser(*args)

    request = args.first.request

    method  = request.method.to_s
    path    = request.path.to_s
    params  = request.query_params.to_h
    headers = request.headers.to_h

    flat_headers = {} of String => String
    headers.each { |header|
        flat_header = {"#{header.first}" => "#{header.last.join(",")}"}
        flat_headers.merge!(flat_header)
    }
    headers = flat_headers.to_a.sort_by! { |key, value| key }.to_h

    host    = headers["Host"]

    begin
        body = parse_body(args.first)
    rescue
        expected = {"start"=>[] of String,"pause"=>[] of String}
        example = {"start"=>["* 08 * * 1,2,3,4,5"],"pause"=>["* 18 * * 1,2,3,4,5","* * * * 6,7"]}
        error = true
        default = {
          "error" => error ,
          "message" => "bad request" ,
          "received" => body ,
          "expected" => expected ,
          "example" => example,
          "name" => "web-scheduler",
          "description" => "A simple web-based crystal-lang app to check versus multiple cron schedules whether a task should run or not",
          "git" => "https://gitlab.com/awesome_o/scheduler"
        }.to_json
        body = JSON.parse(default)
    end

    response = body.to_pretty_json

    DEBUG == "true" && STDERR.puts response

    return response

end

def parse_body(*args)
    request = args.first.request
    body    = request.body
    headers = request.headers.to_h
    body = JSON.parse(body.as(IO).gets_to_end)
    params  = request.query_params.to_h
    config = body
    begin
      time = Time.parse_rfc3339(params["ts"])
    rescue
      time = Time.utc()
    end
    begin
      format = params["format"]
    rescue
      format = "short"
    end
    now = time.to_rfc3339
    STDERR.puts now
    day_of_week = time.day_of_week.to_s
    last_start = [] of String
    config.as_h["start"].as_a.each { | schedule |
        last_start.push( CronParser.new( schedule.as_s ).last( time.shift(59,0) ).to_rfc3339 )
    }
    last_start = last_start.sort.last
    last_pause = [] of String
    config.as_h["pause"].as_a.each { | schedule |
        last_pause.push( CronParser.new( schedule.as_s ).last( time.shift(59,0) ).to_rfc3339 )
    }
    last_pause = last_pause.sort.last
    next_start = [] of String
    config.as_h["start"].as_a.each { | schedule |
        next_start.push( CronParser.new( schedule.as_s ).next( time.shift(-59,0) ).to_rfc3339 )
    }
    next_start = next_start.sort.first
    next_pause = [] of String
    config.as_h["pause"].as_a.each { | schedule |
        next_pause.push( CronParser.new( schedule.as_s ).next( time.shift(-59,0) ).to_rfc3339 )
    }
    next_pause = next_pause.sort.first
    if [last_pause,last_start].max == last_start
        state = "live"
    else
        state = "paused"
    end
    if now == next_pause
        state = "paused"
    end
    if now == next_start
        state = "live"
    end
    if format == "full"
        schedules = {
          "state"   => state,
          "payload" =>
            {
              "new_status" => state
            },
          "now"     => now,
          "day_of_week" => day_of_week,
          "next"    =>
            {
              "start" => next_start,
              "pause" => next_pause
            },
          "last"  =>
            {
              "start" => last_start,
              "pause" => last_pause
            }
        }
        puts schedules.to_pretty_json
    else
        schedules = {
          "new_status" => state
        }
        puts schedules.to_json
    end
    body = JSON.parse(schedules.to_json)
    return body
end
